# shinok

Q1. 
  To Index in ES :
    POST req in /es with parameter `text`
    code in /api/controller/EsController.js and /api/services/EsServices.js

  To Save in Redis :
    POST req in /redis with parameter `key`, 'value`
    code in /api/controller/RedisController.js and /api/services/RedisServices.js

Q2.
  Search in elasticsearch : 
    GET req in /es with parameter `text`
    code in /api/controller/EsController.js and /api/services/EsServices.js

Q3.
  Search in redis : 
    GET req in /redis with parameter `key`
    code in /api/controller/RedisController.js and /api/services/RedisServices.js

#How to use
Install all npm modules bby running :-
  'npm install'

Install redis server
Install elasticsearch server
Keep them on :)