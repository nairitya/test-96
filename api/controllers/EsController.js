/**
 * EsController
 *
 * @description :: Server-side logic for managing es
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  set: function (req, res) {
    var data = req.param('text')
    if (!data) {
      return res.send('You forgot to send text', 400)
    }
    EsServices.indexF(data, function (err, resp) {
      if (err)
        return res.send(err, 400)
      return res.send(resp, 200)
    })
  },

  get: function (req, res) {
    var data = req.param('text')
    if (!data) {
      return res.send('You forgot to send text', 400)
    }
    EsServices.getD(data, function (err, resp) {
      if (err)
        return res.send(err, 400)
      return res.send(resp, 200)
    })
  }
}
