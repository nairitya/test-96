/**
 * RedisController
 *
 * @description :: Server-side logic for managing redis
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  set: function (req, res) {
    var key = req.param('key')
    var value = req.param('value')
    if (!key) {
      return res.send('You forgot to send key', 400)
    }

    if (!value) {
      return res.send('You forgot to send value', 400)
    }

    RedisServices.save(key, value, function (err, resp) {
      if (err)
        return res.send(err, 400)
      return res.send(resp, 200)
    })
  },

  geti: function (req, res) {
    var data = req.param('key')
    if (!data) {
      return res.send('You forgot to send text', 400)
    }
    RedisServices.getf(data, function (err, resp) {
      if (err)
        return res.send(err, 400)
      return res.send(resp, 200)
    })
  }
}
