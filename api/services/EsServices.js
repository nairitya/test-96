var elasticsearch = require('elasticsearch')
var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
})

exports.indexF = function (data, cb) {
  client.create({
    index: 'myindex',
    type: 'mytype',
    body: {a: data},
  }, function (error, response) {
    if (error)
      return cb(error, null)
    else
      return cb(null, response)
  })
}

exports.getD = function (data, cb) {
  client.search({
    q: data
  }).then(function (body) {
    var hits = body.hits.hits
    return cb(null, hits)
  }, function (error) {
    return cb(error, null)
  })
}
