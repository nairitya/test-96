var redis = require('redis'),
  client = redis.createClient()

client.on('error', function (err) {
  console.log('Error ' + err)
})

exports.save = function (key, value, cb) {
  client.set(key, value, redis.print)
  return cb(null, '{"' + key + '":"' + value + '"}')
}

exports.getf = function (key, cb) {
  client.get(key, function (err, reply) {
    if (err)
      return cb(err, null)
    else{
      return cb(null, reply)
    }
  })
}
